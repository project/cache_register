CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage
 * How It Works
 * Maintainers


INTRODUCTION
------------

Cache Register aims to improve the developer experience of caching arbitrary data. It does this by providing simple Drawer, Slot, and Register objects that enforce best practice naming of associated cache entries and provide assorted helper methods for interacting with those cache entries.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/895232/ for further information.

CONFIGURATION
-------------

Cache Register has no menu or modifiable settings, there is no configuration, and your ste will not be affected by installation or uninstallation. All it does is provide fancy aliases to methods present on CacheBackendInterface.

USAGE
-----

Let's say you hit an API for a batch of users and want to cache that information. Here's how you'd do it.

```
// Hire your manager
$manager = \Drupal::service('cache_register.manager');

// Open up the drawer in which you want to group the caches for your users.
$some_api_drawer = $manager->openDrawer('my_module', 'some_api');

// An array of your users. Obviously this wouldn't be empty, right?!
$users = [];
foreach ($users as $user) {
  // Add a slot to your drawer for this user's cache entry.
  // Note that this does not create an entry in the cache.
  $user_slot = $some_api_drawer->openSlot($user->id());

  // Maybe you only want to hit the API if the cache is empty
  if (!$user_slot->isCached() {
    // Hit your API
    $user_api_data = some_api_request($user->id());

    // Populate the cache.
    // setCache() can also set expire time and tags, like a normal $cache->set()
    $user_slot->setCache($user_api_data);
    $user_slot->addCacheTags("user:{$user->id()}");
  }
}

// Want to grab that data elsewhere in the site?
// Reopen the Drawer/Slot to get the data
$user_slot = $manager->openSlot('my_module', 'some_api', $user->id());
// or
$some_api_drawer = $manager->openDrawer('my_module', 'some_api');
$user_slot = $some_api_drawer->openSlot($user->id());
// then either of
$user_api_data = $user_slot->getCache()->data;
$user_api_data = $user_slot->getCacheData();

// Later want to invalidate all cache entries associated the slots in a drawer?
$some_api_drawer->invalidateSlots();
```

When opening up a Drawer, you also have the option of opening up a corresponding Register. A Register is a special Slot whose cache entry tracks the IDs of all Slots opened on the Drawer. To open the register, you just pass an extra parameter when opening your item, like so:

```
// One of
$drawer = $manager->openDrawer('my_module', 'drawer_id', TRUE);
$slot = $manager->openSlot('my_module', 'drawer_id', 'slot_id, TRUE);
// then one of
$slot_list= $drawer->getRegister()->getList();
$slot_list = $slot->getRegister()->getList();
```

Note that opening a register has performance/storage implications since an array item is added to it's cache entry for every slot, so exercise caution when using this feature. If you aren't going to be specifically accessing the register items then it's probably best not to open one.

HOW IT WORKS
------------

It's important to note that a Drawer does not actually point to any entry in the cache, but instead provides a way to associate groups of related cache entries. When you do this:

```
$manager = \Drupal::service('cache_register.manager');
$some_api_drawer = $manager->openDrawer('my_module', 'some_api');
$user_slot = $some_api_drawer->openSlot(1234);
$user_slot->setCache($api_data);
```

setCache() is looking at the ID of the drawer and the ID of the slot to determine the ID of the cache entry (cid) -- in this case my_module.some_api:1234 -- on which to save $api_data.

Further, the cache entry for every slot opened on a drawer is given the cache tag drawer:Drawer->id() -- in our above example this would be drawer:my_module.some_api. This means that you can open up a given drawer anywhere across your codebase, and subsequently run $drawer->invalidateSlots() to invalidate all entries associated with the Drawer and its Slots.

MAINTAINERS
-----------

Current maintainers:
 * Matthew Weiner (mrweiner) - https://www.drupal.org/u/mrweiner

Supporting organizations:
 * Flat 9 - https://www.drupal.org/flat-9
