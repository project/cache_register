<?php

namespace Drupal\cache_register\Exception;

/**
 * Defines a base error class for cache_register errors.
 */
class CacheRegisterError extends \Error {

}
