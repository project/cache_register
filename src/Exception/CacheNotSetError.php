<?php

namespace Drupal\cache_register\Exception;

/**
 * For when a method is called on a cache that is not set.
 */
class CacheNotSetError extends CacheRegisterError {

}
