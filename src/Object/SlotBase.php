<?php

namespace Drupal\cache_register\Object;

use Drupal\cache_register\Exception\CacheNotSetError;
use Drupal\Component\Assertion\Inspector;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * A base class for all Slots.
 */
abstract class SlotBase implements SlotBaseInterface {

  public const INVALIDATED_STATIC_KEY = 'invalidated';

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The parent CacheRegister.
   *
   * @var \Drupal\cache_register\Object\DrawerInterface
   */
  protected $drawer;

  /**
   * The name of the item in the cache.
   *
   * @var string
   */
  protected $id;

  /**
   * T.
   *
   * @var string
   */
  protected $drawerCacheTag;

  /**
   * The constructor.
   *
   * @param DrawerInterface $parent_drawer
   *   The parent CacheRegister object.
   * @param array|string|int|mixed $slot_ids
   *   The ID(s) to use for the slot's cache identifier.
   */
  public function __construct(DrawerInterface $parent_drawer, $slot_ids) {
    $this->cache = $parent_drawer->getCacheBackend();
    $this->drawer = $parent_drawer;
    $this->id = $this->constructCacheEntryId($slot_ids);
  }

  /**
   * {@inheritDoc}
   */
  public function id(): string {
    return $this->id;
  }

  /**
   * {@inheritDoc}
   */
  public function getDrawer(): DrawerInterface {
    return $this->drawer;
  }

  /**
   * {@inheritDoc}
   */
  public function getRegister(): ?RegisterInterface {
    return $this->getDrawer()->getRegister();
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheBackend(): CacheBackendInterface {
    return $this->cache;
  }

  /**
   * {@inheritDoc}
   */
  public function isCached(): bool {
    return !!$this->cache->get($this->id);
  }

  /**
   * {@inheritDoc}
   */
  public function getCache($allow_invalid = FALSE): ?object {
    $cache_item = $this->cache->get($this->id, $allow_invalid);
    $drawer_static = drupal_static($this->id);
    $slot_static = $drawer_static[$this->id] ?? NULL;

    if(!$cache_item) {
      if(!$slot_static) {
        return null;
      }

      // If the cache value was set to null, drupal will treat
      // the cache item as not existing. We therefore need to
      // do the same if the static data has been set to null.
      $slot_static_is_null = property_exists($slot_static, 'data')
        && $slot_static->data === null;
      if($slot_static_is_null) {
        return null;
      }

      $static_is_invalidated = property_exists($slot_static, self::INVALIDATED_STATIC_KEY)
        && $slot_static->{self::INVALIDATED_STATIC_KEY} === true;
      $static_is_valid = $allow_invalid || !$static_is_invalidated;
      return $static_is_valid ? $slot_static : null;
    }

    return $cache_item;
  }

  /**
   * {@inheritDoc}
   */
  public function addCacheTags($tags): void {
    if (!$cache_item = $this->getCache()) {
      throw new CacheNotSetError("Cannot add tags to Slot {$this->id}: Its cache has not been set.");
    }

    if (!is_string($tags)) {
      assert(Inspector::assertTraversable($tags), 'Cache tags must be valid strings');
      assert(Inspector::assertAllStrings($tags), 'Cache tags must be valid strings');
    }

    if (is_string($tags)) {
      $tags = [$tags];
    }

    $new_tags = is_string($tags) ? [$tags] : $tags;

    /** @var array $existing_tags */
    $existing_tags = $cache_item->tags;
    $updated_tags = Cache::mergeTags($new_tags, $existing_tags);

    // Do not use $this->setCache() here.
    //  - We do not need to worry about setting the register.
    //  - setCache() causes tests to fail, because the method
    //    is abstract on BaseSlot and nothing will happen.
    $this->doSetCache($cache_item->data, $cache_item->expire, $updated_tags);
  }

  /**
   * {@inheritDoc}
   */
  abstract public function setCache($data, int $expire = CacheBackendInterface::CACHE_PERMANENT, array $tags = []): void;

  /**
   * Common inner method for abstracted setCache.
   *
   * @param mixed $data
   *   The data to be saved to the given cache slot.
   * @param int $expire
   *   One of the following values:
   *   - CacheBackendInterface::CACHE_PERMANENT: Indicates that the item should
   *     not be removed unless it is deleted explicitly.
   *   - A Unix timestamp: Indicates that the item will be considered invalid
   *     after this time, i.e. it will not be returned by get() unless
   *     $allow_invalid has been set to TRUE. When the item has expired, it may
   *     be permanently deleted by the garbage collector at any time.
   * @param array $tags
   *   An array of tags to be stored with the cache item. These should normally
   *   identify objects used to build the cache item, which should trigger
   *   cache invalidation when updated. For example if a cached item represents
   *   a node, both the node ID and the author's user ID might be passed in as
   *   tags. For example ['node:123', 'node:456', 'user:789'].
   *
   * @see CacheBackendInterface
   */
  protected function doSetCache($data, int $expire, array $tags): void {
    $tags = Cache::mergeTags($tags, $this->getDrawer()->getCacheTags());
    $drawer_static = &drupal_static($this->drawer->id());
    $drawer_static ??= [];

    $drawer_static[$this->id] = (object) [
      'data' => $data,
      'expire' => $expire,
      'tags' => $tags
    ];
    $this->cache->set($this->id, $data, $expire, $tags);
  }

  /**
   * Gets the data from the slot's cache entry.
   *
   * Do not use this method if the data itself is
   * a NULL, as there is no way to tell whether
   * NULL was returned because the cache is not set
   * or because the value of the data is NULL.
   *
   * @param bool $allow_invalid
   *   (optional) If TRUE, a cache item may be returned even if it is expired or
   *   has been invalidated. Such items may sometimes be preferred, if the
   *   alternative is recalculating the value stored in the cache, especially
   *   if another concurrent request is already recalculating the same value.
   *   The "valid" property of the returned object indicates whether the item is
   *   valid or not. Defaults to FALSE.
   *
   * @return mixed
   *   The cache item's data or NULL on failure.
   */
  abstract protected function getCacheData(bool $allow_invalid = FALSE);

  /**
   * Common inner method for abstracted setCache.
   *
   * @param bool $allow_invalid
   *   The allow invalid.
   *
   * @return mixed
   *   Mixed value.
   */
  protected function doGetCacheData(bool $allow_invalid) {
    $cache_item = $this->getCache($allow_invalid);
    return is_null($cache_item) ? NULL : $cache_item->data;
  }

  /**
   * Constructs the slot's cache ID.
   *
   * Uses the parent register name and the provided IDs.
   *
   * @param string|int|string[]|int[] $slot_cache_ids
   *   One ID, or a set of IDs, used to identify the cache item.
   *
   * @return string
   *   Cache ID String
   *
   * @throws \TypeError
   */
  private function constructCacheEntryId($slot_cache_ids): string {
    try {
      $this->validateSlotCacheIds($slot_cache_ids);
    }
    catch (\TypeError $e) {
      throw $e;
    }
    $cache_id = is_array($slot_cache_ids) ? implode('.', $slot_cache_ids) : $slot_cache_ids;
    return "{$this->drawer->id()}:$cache_id";
  }

  /**
   * Checks that the cache IDs are valid types.
   *
   * @param int|string|array $slot_cache_ids
   *   The IDs.
   *
   * @throws \TypeError
   */
  private function validateSlotCacheIds($slot_cache_ids) {
    try {
      $this->validateStringIntArray($slot_cache_ids);

      if (is_array($slot_cache_ids)) {
        foreach ($slot_cache_ids as $id) {
          $this->validateStringIntArray($id);
        }
      }
    }
    catch (\TypeError $e) {
      throw new \TypeError('$slot_cache_ids must be either a string or int, or array of strings and/or ints.');
    }
  }

  /**
   * Validate the string int array.
   *
   * @param mixed $to_check
   *   Item to validate.
   *
   * @return bool
   *   TRUE if valid, else error.
   *
   * @throws \TypeError
   */
  private function validateStringIntArray($to_check): bool {
    if (!is_string($to_check)
      && !is_int($to_check)
      && !is_array($to_check)) {
      throw new \TypeError();
    }

    return TRUE;
  }

}
