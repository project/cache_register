<?php

namespace Drupal\cache_register\Object;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Provides a common parent for related slots and their register.
 *
 * A drawer should never be instantiated directly by an end user.
 * Use \Drupal::service('cache_register.manager')->openDrawer().
 */
class Drawer implements DrawerInterface {

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * This Drawer's identifier.
   *
   * @var string
   */
  protected $id;

  /**
   * The cache tags associated with the drawer.
   *
   * @var array
   */
  protected $cacheTags = [];

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache backend.
   * @param string $drawer_id
   *   Prefixes the cache entry IDs of child Slots.
   *   This should generally be prefixed by the implementor's name,
   *   so something like my_module.bookshelf.
   * @param bool $open_register_if_inactive
   *   Whether or not to open a register for the drawer.
   *   If a register is already active then the register will
   *   always be opened.
   *
   *   It's possible that keeping a register open for a large
   *   set of slots could result in performance penalties.
   *   Use this with caution if you know this Drawer is likely
   *   to contain a large number of slots.
   */
  public function __construct(CacheBackendInterface $cache, string $drawer_id, bool $open_register_if_inactive) {
    $this->cache = $cache;
    $this->id = $drawer_id;
    $this->populateCacheTags();
    if ($this->hasActiveRegister() || $open_register_if_inactive) {
      $this->openRegister();
    }
  }

  /**
   * {@inheritDoc}
   */
  public function id(): string {
    return $this->id;
  }

  /**
   * {@inheritDoc}
   */
  public function hasActiveRegister(bool $allow_invalidated = FALSE): bool {
    return !!$this->cache->get($this->id . ":register");
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheBackend(): CacheBackendInterface {
    return $this->cache;
  }

  /**
   * {@inheritDoc}
   */
  public function getRegister($allow_invalidated = FALSE): ?RegisterInterface {
    return $this->hasActiveRegister($allow_invalidated) ? $this->openRegister() : NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheTags(): array {
    return $this->cacheTags;
  }

  /**
   * {@inheritDoc}
   */
  public function openSlot($slot_ids): SlotInterface {
    return new Slot($this, $slot_ids);
  }

  /**
   * {@inheritDoc}
   */
  public function invalidate($reopen_register = FALSE) {
    Cache::invalidateTags($this->cacheTags);

    $drawer_static = &drupal_static($this->id);
    if(is_array($drawer_static)) {
      foreach($drawer_static as $slot_id => $slot_data) {
        $slot_static_arr = (array) $slot_data;
        $slot_static_arr[SlotBase::INVALIDATED_STATIC_KEY] = true;
        $drawer_static[$slot_id] = (object) $slot_static_arr;
      }
    }

    if ($reopen_register) {
      $this->openRegister();
    }
  }

  /**
   * Attaches cache tags strings to our drawer.
   *
   * The drawer is not an actual cache entry, so it does
   * not actually leverage these tags itself. Instead,
   * the tags are appended to cache entries associated with
   * any of it's slots.
   */
  private function populateCacheTags() {
    // If more tags are needed, can use Cache::buildTags.
    $this->cacheTags[] = 'drawer:' . $this->id;
  }

  /**
   * Opens the associated register.
   */
  private function openRegister() {
    return new Register($this);
  }

}
