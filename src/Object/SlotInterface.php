<?php

namespace Drupal\cache_register\Object;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Defines and interface for Slot.
 */
interface SlotInterface extends SlotBaseInterface {

  /**
   * Sets the slot's cache entry only if the cache isn't set.
   *
   * @param mixed $data
   *   The data to be saved to the given cache slot.
   * @param int $expire
   *   One of the following values:
   *   - CacheBackendInterface::CACHE_PERMANENT: Indicates that the item should
   *     not be removed unless it is deleted explicitly.
   *   - A Unix timestamp: Indicates that the item will be considered invalid
   *     after this time, i.e. it will not be returned by get() unless
   *     $allow_invalid has been set to TRUE. When the item has expired, it may
   *     be permanently deleted by the garbage collector at any time.
   * @param array $tags
   *   An array of tags to be stored with the cache item. These should normally
   *   identify objects used to build the cache item, which should trigger
   *   cache invalidation when updated. For example if a cached item represents
   *   a node, both the node ID and the author's user ID might be passed in as
   *   tags. For example ['node:123', 'node:456', 'user:789'].
   *
   * @see CacheBackendInterface
   */
  public function setCacheStrict($data, int $expire = CacheBackendInterface::CACHE_PERMANENT, array $tags = []): void;

  /**
   * Gets the data from the slot's cache entry.
   *
   * Do not use this method if the data itself is
   * a NULL, as there is no way to tell whether
   * NULL was returned because the cache is not set
   * or because the value of the data is NULL.
   *
   * @param bool $allow_invalid
   *   (optional) If TRUE, a cache item may be returned even if it is expired or
   *   has been invalidated. Such items may sometimes be preferred, if the
   *   alternative is recalculating the value stored in the cache, especially
   *   if another concurrent request is already recalculating the same value.
   *   The "valid" property of the returned object indicates whether the item is
   *   valid or not. Defaults to FALSE.
   *
   * @return mixed
   *   The cache item's data or NULL on failure.
   */
  public function getCacheData(bool $allow_invalid = FALSE);

  /**
   * Delete the slot's cache entry.
   */
  public function deleteCache(): void;

  /**
   * Delete the slot's cache entry.
   */
  public function invalidateCache(): void;

}
