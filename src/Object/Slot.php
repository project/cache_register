<?php

namespace Drupal\cache_register\Object;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Provides an accessor for a specific cache entry.
 *
 * A slot should never be instantiated directly by an end user.
 * Use \Drupal::service('cache_register.manager')->openSlot()
 * or use the openSlot method on a Drawer.
 */
class Slot extends SlotBase implements SlotInterface {

  /**
   * {@inheritDoc}
   */
  public function setCache($data, $expire = CacheBackendInterface::CACHE_PERMANENT, $tags = []): void {
    $this->addToRegister();
    $this->doSetCache($data, $expire, $tags);
  }

  /**
   * {@inheritDoc}
   */
  public function setCacheStrict($data, int $expire = CacheBackendInterface::CACHE_PERMANENT, array $tags = []): void {
    if (!$this->isCached()) {
      $this->setCache($data, $expire, $tags);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheData(bool $allow_invalid = FALSE) {
    return $this->doGetCacheData($allow_invalid);
  }

  /**
   * {@inheritDoc}
   */
  public function deleteCache(): void {
    $this->cache->delete($this->id);
    $drawer_static = &drupal_static($this->id);
    $drawer_static[$this->id] = null;
    $this->removeFromRegister();
  }

  /**
   * {@inheritDoc}
   */
  public function invalidateCache(): void {
    $this->cache->invalidate($this->id);

    $drawer_static = &drupal_static($this->id);
    if(isset($drawer_static[$this->id])) {
      $slot_static_arr = (array) $drawer_static[$this->id];
      $slot_static_arr[SlotBase::INVALIDATED_STATIC_KEY] = true;
      $drawer_static[$this->id] = (object) $slot_static_arr;
    }

    $this->removeFromRegister();
  }

  /**
   * Adds the slot to the register.
   */
  private function addToRegister(): void {
    if ($register = $this->getDrawer()->getRegister()) {
      $register->addSlot($this);
    }
  }

  /**
   * Removes the slot from the register.
   */
  private function removeFromRegister(): void {
    if ($register = $this->getDrawer()->getRegister()) {
      $register->removeSlot($this);
    }
  }

}
