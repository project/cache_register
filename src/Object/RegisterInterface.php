<?php

namespace Drupal\cache_register\Object;

/**
 * Defines an interface for Register.
 */
interface RegisterInterface extends SlotBaseInterface {

  /**
   * Gets the list of Slot IDs in the register.
   *
   * This is an alias for getCacheData().
   *
   * @return array|null
   *   The list of Slot IDs.
   */
  public function getList(): ?array;

  /**
   * Add a slot to the register.
   *
   * @param \Drupal\cache_register\Object\SlotInterface $slot_to_add
   *   Add slot.
   */
  public function addSlot(SlotInterface $slot_to_add): void;

  /**
   * Remove a slot from the register.
   *
   * @param \Drupal\cache_register\Object\SlotInterface $slot_to_remove
   *   Remove slot.
   */
  public function removeSlot(SlotInterface $slot_to_remove): void;

}
