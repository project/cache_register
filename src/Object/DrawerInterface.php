<?php

namespace Drupal\cache_register\Object;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Defines an interface for Drawer.
 */
interface DrawerInterface {

  /**
   * Id method.
   *
   * @return string
   *   The drawer's name.
   */
  public function id(): string;

  /**
   * Checks if a register exists for the drawer.
   *
   * @param bool $allow_invalidated
   *   Whether or not to include an invalidated register.
   *   This can likely be ignored unless the drawer has
   *   invalidated its slots.
   *
   * @return bool
   *   Whether or not the register exists (is cached).
   */
  public function hasActiveRegister(bool $allow_invalidated = FALSE): bool;

  /**
   * Gets the Cache Backend associated with the Drawer.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The Cache Backend.
   */
  public function getCacheBackend(): CacheBackendInterface;

  /**
   * Gets the Drawer's Register.
   *
   * @return \Drupal\cache_register\Object\RegisterInterface|null
   *   Returns the drawer's register, or NULL if not initialized.
   */
  public function getRegister(): ?RegisterInterface;

  /**
   * Gets the cache tags associated with the drawer.
   *
   * @return array
   *   The cache tag array.
   */
  public function getCacheTags(): array;

  /**
   * Creates a new Slot in the Drawer.
   *
   * @param array|string $slot_ids
   *   The ID(s) to use for the slot's cache identifier.
   *   This might be something like a UID if you are
   *   caching information about a specific user.
   *
   * @note This does note create/populate the associated
   *   cache entry. You must implement $slot->set($data)
   *   to populate its cache entry.
   *
   * @return \Drupal\cache_register\Object\SlotInterface
   *   Returns a Slot in the Drawer.
   */
  public function openSlot($slot_ids): SlotInterface;

  /**
   * Invalidate all slots in the drawer.
   *
   * @param bool $reopen_register
   *   After invalidation, reinstantiate the empty register.
   *
   * @return mixed
   *   Returns mixed value.
   */
  public function invalidate($reopen_register = FALSE);

}
