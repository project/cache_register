<?php

namespace Drupal\cache_register\Object;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Defines an interface for SlotBase.
 */
interface SlotBaseInterface {

  /**
   * Get the ID of the slot's cache entry.
   *
   * @return string
   *   The ID.
   */
  public function id(): string;

  /**
   * Gets the slots parent drawer.
   *
   * @return \Drupal\cache_register\Object\DrawerInterface
   *   The drawer.
   */
  public function getDrawer(): DrawerInterface;

  /**
   * Gets the slot's drawer's register.
   *
   * @return \Drupal\cache_register\Object\DrawerInterface|null
   *   The drawer.
   */
  public function getRegister(): ?RegisterInterface;

  /**
   * Gets the cache backend.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The cache backend.
   */
  public function getCacheBackend(): CacheBackendInterface;

  /**
   * Checks if the slot has an entry in the cache.
   *
   * @return bool
   *   Whether or not the slot with the given ids is cached.
   */
  public function isCached(): bool;

  /**
   * Gets the slot's cache entry.
   *
   * @param bool $allow_invalid
   *   (optional) If TRUE, a cache item may be returned even if it is expired or
   *   has been invalidated. Such items may sometimes be preferred, if the
   *   alternative is recalculating the value stored in the cache, especially
   *   if another concurrent request is already recalculating the same value.
   *   The "valid" property of the returned object indicates whether the item is
   *   valid or not. Defaults to FALSE.
   *
   * @return object|null
   *   The cache item or NULL on failure.
   */
  public function getCache(bool $allow_invalid = FALSE): ?object;

  /**
   * Adds the given tags to the slot's cache entry.
   *
   * @param array|string $tags
   *   One or multiple tags to append to the item.
   *
   * @throws \Throwable
   *   If the cache hasn't been set.
   */
  public function addCacheTags($tags): void;

  /**
   * Sets the slot's cache entry.
   *
   * @param mixed $data
   *   The data to be saved to the given cache slot.
   * @param int $expire
   *   One of the following values:
   *   - CacheBackendInterface::CACHE_PERMANENT: Indicates that the item should
   *     not be removed unless it is deleted explicitly.
   *   - A Unix timestamp: Indicates that the item will be considered invalid
   *     after this time, i.e. it will not be returned by get() unless
   *     $allow_invalid has been set to TRUE. When the item has expired, it may
   *     be permanently deleted by the garbage collector at any time.
   * @param array $tags
   *   An array of tags to be stored with the cache item. These should normally
   *   identify objects used to build the cache item, which should trigger
   *   cache invalidation when updated. For example if a cached item represents
   *   a node, both the node ID and the author's user ID might be passed in as
   *   tags. For example ['node:123', 'node:456', 'user:789'].
   *
   * @see CacheBackendInterface
   */
  public function setCache($data, int $expire = CacheBackendInterface::CACHE_PERMANENT, array $tags = []): void;

}
