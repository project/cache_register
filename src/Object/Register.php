<?php

namespace Drupal\cache_register\Object;

use Drupal\Core\Cache\CacheBackendInterface;
use http\Exception\InvalidArgumentException;

/**
 * Maintains a cache entry that tracks all slots opened on Drawer.
 *
 * A drawer should never be instantiated directly by an end user.
 * Use \Drupal::service('cache_register.manager')->openRegister()
 * or the openRegister() method on a Drawer or a Slot.
 */
class Register extends SlotBase implements RegisterInterface {

  /**
   * The constructor.
   *
   * @param \Drupal\cache_register\Object\DrawerInterface $drawer_to_track
   *   The drawer that this register tracks.
   */
  public function __construct(DrawerInterface $drawer_to_track) {
    parent::__construct($drawer_to_track, 'register');
    if (!$this->isCached()) {
      $this->setCache([]);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getList(): ?array {
    return $this->getCacheData();
  }

  /**
   * {@inheritDoc}
   */
  public function addSlot(SlotInterface $slot_to_add): void {
    $this->addRemoveSlot('add', $slot_to_add);
  }

  /**
   * {@inheritDoc}
   */
  public function removeSlot(SlotInterface $slot_to_remove): void {
    $this->addRemoveSlot('remove', $slot_to_remove);
  }

  /**
   * {@inheritDoc}
   */
  public function setCache($data, $expire = CacheBackendInterface::CACHE_PERMANENT, $tags = []): void {
    $this->doSetCache($data, $expire, $tags);
  }

  /**
   * {@inheritDoc}
   */
  protected function getCacheData(bool $allow_invalid = FALSE) {
    return $this->doGetCacheData($allow_invalid);
  }

  /**
   * Add or remove a slot from the register.
   *
   * @param string $add_or_remove
   *   Either 'add' or 'remove'.
   * @param \Drupal\cache_register\Object\SlotInterface $slot_to_add_remove
   *   The slot we are adding or removing.
   */
  protected function addRemoveSlot(string $add_or_remove, SlotInterface $slot_to_add_remove): void {
    $valid_options = ['add', 'remove'];
    if (!in_array($add_or_remove, $valid_options)) {
      throw new InvalidArgumentException('$add_or_remove must be set to either "add" or "remove"');
    }

    $register_cache = $this->getCache();
    $register_list = $register_cache->data;
    if ($add_or_remove === 'add') {
      $register_list[$slot_to_add_remove->id()] = $slot_to_add_remove->id();
    }
    else {
      unset($register_list[$slot_to_add_remove->id()]);
    }

    $this->setCache($register_list, $register_cache->expire, $register_cache->tags);
  }

}
