<?php

namespace Drupal\cache_register;

use Drupal\cache_register\Object\Drawer;
use Drupal\cache_register\Object\DrawerInterface;
use Drupal\cache_register\Object\RegisterInterface;
use Drupal\cache_register\Object\SlotInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Provides methods for instantiating Cache Register objects.
 */
class Manager implements ManagerInterface {

  /**
   * Drupal\Core\Cache\CacheBackendInterface definition.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Constructs a new CacheRegisterFactory object.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The CacheBackendInterface.
   */
  public function __construct(CacheBackendInterface $cache) {
    $this->cache = $cache;
  }

  /**
   * {@inheritDoc}
   */
  public function openSlot(string $implementor_id, $drawer_name, $slot_ids, bool $open_register_if_inactive = FALSE): SlotInterface {
    $drawer = $this->openDrawer($implementor_id, $drawer_name, $open_register_if_inactive);
    return $drawer->openSlot($slot_ids);
  }

  /**
   * {@inheritDoc}
   */
  public function openDrawer(string $implementor_id, $drawer_name, bool $open_register_if_inactive = FALSE): DrawerInterface {
    $drawer_id = $implementor_id;

    $bucket_suffix = is_array($drawer_name)
      ? implode('.', $drawer_name)
      : $drawer_name;
    $drawer_id .= ".$bucket_suffix";

    return new Drawer($this->cache, $drawer_id, $open_register_if_inactive);
  }

  /**
   * {@inheritDoc}
   */
  public function openRegister(string $implementor_id, $drawer_name): RegisterInterface {
    $drawer = $this->openDrawer($implementor_id, $drawer_name, TRUE);
    return $drawer->getRegister();
  }

}
