<?php

namespace Drupal\cache_register;

use Drupal\cache_register\Object\DrawerInterface;
use Drupal\cache_register\Object\RegisterInterface;
use Drupal\cache_register\Object\SlotInterface;

/**
 * Defines an interface for Manager.
 */
interface ManagerInterface {

  /**
   * Creates a Slot object and its Drawer.
   *
   * @param string $implementor_id
   *   This should generally be the implementing module's name.
   *   It will be used as the beginning of all child slots'
   *   cache entry IDs.
   * @param string|string[] $drawer_name
   *   A string or array of strings used to identify
   *   the parent Drawer.
   * @param string|int|array<string|int> $slot_ids
   *   A string or int, or array of strings/ints, that will be used
   *   to identify the slot and its corresponding cache entry.
   * @param bool $open_register_if_inactive
   *   (optional) Whether to open a register for the drawer.
   *   If the drawer already has an active register then it will
   *   always be opened regardless of this param's value.
   *
   *   It's possible that keeping a register open for a large
   *   set of slots could result in performance penalties.
   *   We don't know! Use this with caution if you know this
   *   Drawer is likely to open a large number of slots.
   *   As such, this defaults to FALSE.
   *
   * @return \Drupal\cache_register\Object\SlotInterface
   *   Returns the slot object.
   */
  public function openSlot(string $implementor_id, $drawer_name, $slot_ids, bool $open_register_if_inactive = FALSE): SlotInterface;

  /**
   * Creates a Drawer object.
   *
   * @param string $implementor_id
   *   This should generally be the implementing module's name.
   *   Used to construct the cache IDs for the Drawer's slots.
   * @param string|array $drawer_name
   *   A string or array of strings used to identify the drawer.
   *   The name(s) should reflect the data the drawer contains.
   *   Used to construct the cache IDs for the Drawer's slots.
   * @param bool $open_register_if_inactive
   *   (optional) Whether or not to open a register for the drawer.
   *   If the drawer already has an active register then it will
   *   always be opened regardless of this param's value.
   *
   *   It's possible that keeping a register open for a large
   *   set of slots could result in performance penalties.
   *   We don't know! Use this with caution if you know this
   *   Drawer is likely to open a large number of slots.
   *   As such, this defaults to FALSE.
   *
   * @return \Drupal\cache_register\Object\DrawerInterface
   *   Returns the Drawer object.
   */
  public function openDrawer(string $implementor_id, $drawer_name, bool $open_register_if_inactive = FALSE): DrawerInterface;

  /**
   * Creates a Register object and its Drawer.
   *
   * @param string $implementor_id
   *   This should generally be the implementing module's name.
   *   Used to construct the cache IDs for the Drawer's slots.
   * @param string|array $drawer_name
   *   A string or array of strings used to identify the drawer.
   *   The name(s) should reflect the data the drawer contains.
   *   Used to construct the cache IDs for the Drawer's slots.
   *
   * @return \Drupal\cache_register\Object\RegisterInterface
   *   Returns the register object.
   */
  public function openRegister(string $implementor_id, $drawer_name): RegisterInterface;

}
