<?php

namespace Drupal\Tests\cache_register\Kernel;

use Drupal\cache_register\Object\Drawer;
use Drupal\cache_register\Object\DrawerInterface;
use Drupal\cache_register\Object\Register;
use Drupal\cache_register\Object\RegisterInterface;
use Drupal\cache_register\Object\Slot;
use Drupal\cache_register\Object\SlotBaseInterface;
use Drupal\cache_register\Object\SlotInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\KernelTests\Core\Cache\DatabaseBackendTest;

/**
 * Defines a base class for cache_register test classes.
 *
 * @group cache_register
 */
abstract class CacheRegisterKernelTestBase extends DatabaseBackendTest {

  protected const MOCK_DRAWER_ID = 'cache_register.test_drawer';

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  protected static $modules = [
    'cache_register',
  ];

  /**
   * Provides the Manager service.
   *
   * In some cases, tests that instantiate our objects directly
   * throw a Database Serialization error. In those cases,
   * the manager should be used instead.
   *
   * @returns \Drupal\cache_register\Manager
   */
  public function getManager() {
    return \Drupal::service('cache_register.manager');
  }

  /**
   * Creates a Test Drawer.
   *
   * @param string $drawer_id
   *   The Drawer ID.
   * @param bool $open_register
   *   Whether or not the open the register.
   *
   * @return \Drupal\cache_register\Object\DrawerInterface
   *   Returns the drawer object.
   */
  public function createDrawer($drawer_id = '', $open_register = FALSE): DrawerInterface {
    $drawer_id = $drawer_id ?: self::MOCK_DRAWER_ID;
    return new Drawer($this->getCacheBackend(), $drawer_id, $open_register);
  }

  /**
   * Creates a SlotBase for testing.
   *
   * @param string $slot_ids
   *   The slot IDs.
   * @param \Drupal\cache_register\Object\DrawerInterface|null $drawer
   *   The drawer.
   *
   * @return \Drupal\cache_register\Object\SlotBaseInterface|object
   *   Returns the slotbase object.
   */
  public function createSlotBase($slot_ids = 'sid', $drawer = NULL): SlotBaseInterface {
    return $this->getMockForAbstractClass(
      'Drupal\cache_register\Object\SlotBase',
      [
        $this->slotTestDrawerValidatorProvider($drawer),
        $slot_ids,
      ]
    );
  }

  /**
   * To help us test protected/private methods.
   *
   * @param object $obj
   *   The object instance.
   * @param string $method_name
   *   The Method name.
   * @param array $args
   *   The args to pass into the method.
   *
   * @return mixed
   *   Returns the mixed.
   *
   * @throws \ReflectionException
   */
  public static function callMethod(object $obj, string $method_name, array $args = []) {
    $class = new \ReflectionClass($obj);
    $method = $class->getMethod($method_name);
    $method->setAccessible(TRUE);
    return $method->invokeArgs($obj, $args);
  }

  /**
   * Creates a Slot for testing.
   *
   * @param int $slot_ids
   *   The slot IDs.
   * @param \Drupal\cache_register\Object\DrawerInterface|null $drawer
   *   The drawer.
   *
   * @return \Drupal\cache_register\Object\SlotInterface
   *   Retirns the slot object.
   */
  public function createSlot($slot_ids, $drawer = NULL): SlotInterface {
    return new Slot($this->slotTestDrawerValidatorProvider($drawer), $slot_ids);
  }

  /**
   * Creates a Register for testing.
   *
   * @param \Drupal\cache_register\Object\DrawerInterface|null $drawer
   *   The drawer.
   *
   * @return \Drupal\cache_register\Object\RegisterInterface
   *   The register.
   */
  public function createRegister($drawer = NULL): RegisterInterface {
    return new Register($this->slotTestDrawerValidatorProvider($drawer));
  }

  /**
   * Provider for simple bool tests.
   */
  public function boolProvider() {
    return [
      [TRUE],
      [FALSE],
    ];
  }

  /**
   * Provides data to test setCache().
   *
   * @return array[]
   *   Returns the array values.
   */
  public function cacheDataProvider(): array {
    return [
      ['string data'],
      [['array', 'of', 'data']],
      ['string data', strtotime('+1 minute')],
      ['string data', strtotime('+1 month'), ['tag:1', 'tag:2']],
    ];
  }

  /**
   * Provides cache tags bool value.
   *
   * @return array
   *   Returns the array values.
   */
  public function cacheTagsProvider(): array {
    return [
      // Valid.
      ['tag:1', TRUE],
      [['tag:1', 'tag:2'], TRUE],
      // Invalid.
      [new \stdClass(), FALSE],
      [1, FALSE],
      [['tag:1', new \stdClass()], FALSE],
      [['tag:1', 1], FALSE],
    ];
  }

  /**
   * Common method for testing addCacheTags.
   *
   * @param \Drupal\cache_register\Object\SlotBaseInterface $slot
   *   The slot.
   * @param mixed $tags_to_add
   *   The tags.
   * @param bool $is_valid
   *   Whether the provider-provided tags are valid.
   *
   * @return void
   * @throws \ReflectionException
   * @throws \Throwable
   */
  protected function doTestAddCacheTags(SlotBaseInterface $slot, $tags_to_add, bool $is_valid) {
    if (!$is_valid) {
      $this->expectException('AssertionError');
    }
    $initial_data_source = 'some_data';
    $base_tags = ['tag_1', 'tag_2'];
    $this->doSetCache($slot, $initial_data_source, CacheBackendInterface::CACHE_PERMANENT, $base_tags);

    // Ensure the initial cache data was set as expected.
    $this->assertEquals($this->doGetCacheData($slot), $initial_data_source);
    $this->assertSlotTagsMatchExpected($slot, $base_tags);

    $slot->addCacheTags($tags_to_add);

    // Ensure the data didn't change as a result of setting the tags.
    $this->assertEquals($this->doGetCacheData($slot), $initial_data_source);
    $all_tags = Cache::mergeTags($base_tags, is_array($tags_to_add) ? $tags_to_add : [$tags_to_add]);
    $this->assertSlotTagsMatchExpected($slot, $all_tags);

    // Make sure that we test this at the end. If we test this
    // any earlier then the exception stops further execution and
    // subsequent tests will not run.
    $this->callMethod($slot, 'doSetCache', [NULL, 0, []]);
    $this->expectException('Drupal\cache_register\Exception\CacheNotSetError');
    $slot->addCacheTags($tags_to_add);
  }

  /**
   * Common method for testing setCacheData.
   */
  protected function commonTestSetCache($slot, $data, int $expire, array $tags, $method_name = 'setCache') {
    $cid = $slot->id();

    switch ($method_name) {
      case 'doSetCache':
        $this->doSetCache($slot, $data, $expire, $tags);
        break;

      case 'setCache':
        $slot->setCache($data, $expire, $tags);
        break;
    }

    $cache_entry = $slot->getCacheBackend()->get($cid);
    $this->assertEquals($cache_entry->data, $data);
    $this->assertEquals($cache_entry->expire, $expire);
    $this->assertSlotTagsMatchExpected($slot, $tags);
  }

  /**
   * Shortcut to callMethod doSetCache on a slot via reflection.
   *
   * @param \Drupal\cache_register\Object\SlotBaseInterface $slot
   *   The slot.
   * @param $data
   *   The data to cache.
   * @param int $expire
   *   The cache expiration.
   * @param array $tags
   *   The cache tags.
   *
   * @return void
   * @throws \ReflectionException
   */
  protected function doSetCache(SlotBaseInterface $slot, $data, int $expire = CacheBackendInterface::CACHE_PERMANENT, array $tags = []) {
    $this->callMethod(
      $slot,
      'doSetCache',
      [$data, $expire, $tags]
    );
  }

  /**
   * Shortcut to callMethod doGetCacheData() on a slot via reflection.
   *
   * @param \Drupal\cache_register\Object\SlotBaseInterface $slot
   *   The slot.
   * @param bool $allow_invalid
   *   Whether to allow invalidated entries.
   *
   * @return mixed|null
   *   The cache data or NULL.
   * @throws \ReflectionException
   */
  protected function doGetCacheData(SlotBaseInterface $slot, bool $allow_invalid = FALSE) {
    return $this->callMethod($slot, 'doGetCacheData', [$allow_invalid]);
  }

  /**
   * Whether two items contain all the same items.
   *
   * This does not account for keys.
   *
   * @param array $a
   *   Array 1.
   * @param array $b
   *   Array 2.
   *
   * @return bool
   *   Whether two items contain all the same items.
   */
  protected function arraysContainSameItems(array $a, array $b): bool {
    return !array_diff($a, $b) && !array_diff($b, $a);
  }

  /**
   * Asserts that a slots tags match an expected set.
   *
   * @param \Drupal\cache_register\Object\SlotBaseInterface $slot
   *   The slot.
   * @param array $expected_tags
   *   The expected tags.
   *
   * @return void
   */
  protected function assertSlotTagsMatchExpected(SlotBaseInterface $slot, array $expected_tags) {
    $cache_entry = $slot->getCache();
    // The first tag will be based on the drawer, so we need
    // to append it to the expected tags in order to match
    // the two arrays.
    $provided_tags_plus_default = array_merge($expected_tags, [$cache_entry->tags[0]]);

    $this->assertTrue(!array_diff($provided_tags_plus_default, $cache_entry->tags));
    $this->assertTrue(!array_diff($cache_entry->tags, $provided_tags_plus_default));
  }

  /**
   * Common method for testing getCacheData.
   */
  protected function commonTestGetCacheData($slot) {
    $cid = $slot->id();
    $this->assertEquals(NULL, $slot->getCache());

    // After caching.
    $slot->getCacheBackend()->set($cid, 'cache_data');
    $this->assertNotEquals($slot->getCacheData(), NULL);
    $this->assertEquals(
      $slot->getCache()->data,
      $slot->getCacheData()
    );

    // After invalidating the cache.
    $slot->getCacheBackend()->invalidate($slot->id());
    $this->assertEquals(NULL, $slot->getCacheData());
    $this->assertNotEquals($slot->getCacheData(TRUE), NULL);
    $this->assertEquals(
      $slot->getCache(TRUE)->data,
      $slot->getCacheData(TRUE)
    );

    // Repopulate and then delete the cache.
    $slot->getCacheBackend()->set($cid, 'cache_data');
    $slot->getCacheBackend()->delete($cid);
    $this->assertEquals(NULL, $slot->getCache());
    $this->assertEquals(
      $slot->getCache(TRUE),
      $slot->getCacheData(TRUE)
    );
  }

  /**
   * Validates/provides a drawer for test slots.
   *
   * @param \Drupal\cache_register\Object\DrawerInterface|null $drawer
   *   The drawer, if present.
   *
   * @return \Drupal\cache_register\Object\DrawerInterface
   *   The drawer.
   */
  private function slotTestDrawerValidatorProvider(DrawerInterface $drawer = NULL): DrawerInterface {
    if (is_null($drawer)) {
      $drawer = $this->createDrawer();
    }
    elseif (!is_object($drawer)
      || (strpos(get_class($drawer), 'Drawer') === FALSE)) {
      throw new \TypeError('Drawer must be an object of type Drawer. ' . gettype($drawer) . ':' . get_class($drawer) . ' given.');
    }

    return $drawer;
  }


}
