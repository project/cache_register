<?php

namespace Drupal\Tests\cache_register\Kernel;

/**
 * Tests the Drawer class.
 *
 * @coversDefaultClass \Drupal\cache_register\Object\Drawer
 * @group cache_register
 */
class DrawerTest extends CacheRegisterKernelTestBase {

  /**
   * @covers ::id
   */
  public function testId() {
    $drawer = $this->createDrawer('testId', FALSE);
    $this->assertEquals('testId', $drawer->id());
  }

  /**
   * @covers ::hasActiveRegister
   */
  public function testHasActiveRegister() {
    $drawer = $this->createDrawer('testHasActiveRegister', FALSE);
    $this->assertEquals(FALSE, $drawer->hasActiveRegister());

    $this->createRegister($drawer);
    $this->assertEquals(TRUE, $drawer->hasActiveRegister());
  }

  /**
   * @covers ::getCacheBackend
   */
  public function testGetCacheBackend() {
    $drawer = $this->createDrawer();
    $this->assertInstanceOf('\Drupal\Core\Cache\CacheBackendInterface', $drawer->getCacheBackend());
  }

  /**
   * @covers ::getRegister
   */
  public function testGetRegister() {
    $drawer = $this->createDrawer('cr.d', FALSE);
    $this->assertEquals(NULL, $drawer->getRegister());

    $this->createRegister($drawer);
    $this->assertInstanceOf('\Drupal\cache_register\Object\RegisterInterface', $drawer->getRegister());
  }

  /**
   * @covers ::getCacheTags
   */
  public function testGetCacheTags() {
    $drawer = $this->createDrawer('cr.d', FALSE);
    $this->assertIsArray($drawer->getCacheTags());
    $this->assertNotEmpty($drawer->getCacheTags());
  }

  /**
   * @covers ::openSlot
   */
  public function testOpenSlot() {
    $drawer = $this->createDrawer('cr.d');
    $slot = $drawer->openSlot('slot');
    $this->assertInstanceOf('\Drupal\cache_register\Object\Slot', $slot);
  }

  /**
   * @covers ::invalidate
   * @dataProvider boolProvider
   */
  public function testDrawerInvalidate($reopen_register) {
    $drawer = $this->createDrawer('cr.d', TRUE);
    $slot_1 = $drawer->openSlot('slot_1');
    $slot_2 = $drawer->openSlot('slot_2');

    $slot_1->setCache('slot_1_data');
    $slot_2->setCache('slot_2_data');

    $this->assertNotEquals($drawer->getRegister()->getCache(), NULL);
    $this->assertNotEquals($slot_1->getCache(), NULL);
    $this->assertNotEquals($slot_2->getCache(), NULL);

    $drawer->invalidate($reopen_register);
    if ($reopen_register) {
      $this->assertNotEquals($drawer->getRegister()->getCache(), NULL);
    }
    else {
      $this->assertEquals(NULL, $drawer->getRegister());
    }
    $this->assertEquals(NULL, $slot_1->getCache());
    $this->assertEquals(NULL, $slot_2->getCache());
  }

}
