<?php

namespace Drupal\Tests\cache_register\Kernel;

/**
 * Tests the Register class.
 *
 * @coversDefaultClass \Drupal\cache_register\Object\Register
 * @group cache_register
 */
class RegisterTest extends CacheRegisterKernelTestBase {

  /**
   * Ensure slot cache persists reopening.
   */
  public function testReopenRegister() {
    $slot_1 = $this->createSlot('testReopenRegister');
    $cache_data = 'cached_data';
    $slot_1->setCache($cache_data);
    $slot_cache_1 = $slot_1->getCache();

    // Reopen the slot.
    $slot_2 = $this->createSlotBase('testReopenRegister');
    $slot_cache_2 = $slot_2->getCache();
    $this->assertEquals($slot_cache_1, $slot_cache_2);
  }

  /**
   * @covers ::getList
   */
  public function testGetList() {
    /** @var \Drupal\cache_register\Manager $manager */
    $register = $this->createRegister();
    $this->assertEquals($register->getCacheBackend()->get($register->id())->data, $register->getList());
  }

  /**
   * @covers ::addSlot
   */
  public function testAddSlot() {
    $register = $this->createRegister();
    $slot = $this->createSlot('id');
    $register->addSlot($slot);
    $this->assertArrayHasKey($slot->id(), $register->getList());
  }

  /**
   * @covers ::removeSlot
   */
  public function testRemoveSlot() {
    $register = $this->createRegister();
    $slot = $this->createSlot('id');
    $register->addSlot($slot);
    $this->assertArrayHasKey($slot->id(), $register->getList());
    $register->removeSlot($slot);

    $keys = array_keys($register->getList());
    $this->assertNotContains($slot->id(), $keys);
  }

}
