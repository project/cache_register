<?php

namespace Drupal\Tests\cache_register\Kernel;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Tests the Slot class.
 *
 * @coversDefaultClass \Drupal\cache_register\Object\Slot
 * @group cache_register
 */
class SlotTest extends CacheRegisterKernelTestBase {

  /**
   * Ensure slot cache persists reopening.
   */
  public function testReopenSlot() {
    $slot_1 = $this->createSlot('testReopenSlot');
    $cache_data = 'cached_data';
    $slot_1->setCache($cache_data);
    $slot_cache_1 = $slot_1->getCache();

    // Reopen the slot.
    $slot_2 = $this->createSlotBase('testReopenSlot');
    $slot_cache_2 = $slot_2->getCache();
    $this->assertEquals($slot_cache_1, $slot_cache_2);
  }

  /**
   * @covers ::setCache
   * @dataProvider cacheDataProvider
   * @doesNotPerformAssertions
   */
  public function testSetCache($data, int $expire = CacheBackendInterface::CACHE_PERMANENT, array $tags = []) {
    $slot = $this->createSlot('testIsCached');
    $this->commonTestSetCache($slot, $data, $expire, $tags);
  }

  /**
   * @covers ::getCacheData
   * @doesNotPerformAssertions
   */
  public function testGetCacheData() {
    /** @var \Drupal\cache_register\Manager $manager */
    $manager = $this->getManager();
    $slot = $manager->openSlot('cr', 'd', 's');
    $this->commonTestGetCacheData($slot);
  }

  /**
   * @covers ::setCacheStrict
   * @dataProvider cacheDataProvider
   * @depends      testSetCache
   * @depends      testGetCacheData
   */
  public function testSetCacheStrict() {
    $slot = $this->createSlot('testSetCacheStrict');

    // Set the empty cache.
    $slot->setCacheStrict('valid');
    $this->assertEquals($slot->getCacheData(), 'valid');

    // Try to override existing cache entry.
    $slot->setCacheStrict('invalid');
    $this->assertEquals($slot->getCacheData(), 'valid');
  }

  /**
   * @covers ::deleteCache
   * @dataProvider boolProvider
   */
  public function testDeleteCache(bool $open_register) {
    /** @var \Drupal\cache_register\Manager $manager */
    $manager = $this->getManager();
    $slot = $manager->openSlot('cr', 'd', 's', $open_register);
    $slot->setCache('data');
    $this->assertEquals('data', $slot->getCacheData());
    $slot->deleteCache();
    $this->assertEquals(NULL, $slot->getCacheData());
    if ($open_register) {
      $this->assertNotContains($slot->id(), array_keys($slot->getRegister()->getList()));
    }
  }

  /**
   * @covers ::invalidateCache
   * @dataProvider boolProvider
   */
  public function testInvalidateCache(bool $open_register) {
    /** @var \Drupal\cache_register\Manager $manager */
    $manager = $this->getManager();
    $slot = $manager->openSlot('cr', 'd', 's', $open_register);
    $slot->setCache('data');
    $this->assertEquals('data', $slot->getCacheData());
    $slot->invalidateCache();
    $this->assertEquals(NULL, $slot->getCacheData());
    $this->assertEquals('data', $slot->getCacheData(TRUE));

    if ($open_register) {
      $this->assertNotContains($slot->id(), array_keys($slot->getRegister()->getList()));
    }
  }

  /**
   * Test addCacheTags.
   *
   * Probably overkill to test this here in addition to
   * in SlotBaseTest, but just to be safe.
   * @covers ::addCacheTags
   *
   * @dataProvider cacheTagsProvider
   */
  public function testAddCacheTags($tags_to_add, $is_valid) {
    $slot = $this->createSlot('testAddCacheTags');
    $this->doTestAddCacheTags($slot, $tags_to_add, $is_valid);
  }

}
