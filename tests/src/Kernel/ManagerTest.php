<?php

namespace Drupal\Tests\cache_register\Kernel;

/**
 * Tests the Manager class.
 *
 * @coversDefaultClass \Drupal\cache_register\Manager
 * @group cache_register
 */
class ManagerTest extends CacheRegisterKernelTestBase {

  /**
   * @covers ::openSlot
   * @dataProvider boolProvider
   */
  public function testOpenSlot(bool $open_register) {
    $manager = $this->getManager();
    $slot = $manager->openSlot('cr', 'd', 1, $open_register);
    $this->assertInstanceOf("Drupal\cache_register\Object\SlotInterface", $slot);
    if ($open_register) {
      $this->assertInstanceOf("Drupal\cache_register\Object\RegisterInterface", $slot->getRegister());
    }
    else {
      $this->assertEquals(NULL, $slot->getRegister());
    }
  }

  /**
   * @covers ::openDrawer
   * @dataProvider boolProvider
   */
  public function testOpenDrawer(bool $open_register) {
    $manager = $this->getManager();
    $drawer = $manager->openDrawer('cr', 'd', $open_register);
    $this->assertInstanceOf("Drupal\cache_register\Object\DrawerInterface", $drawer);
    if ($open_register) {
      $this->assertInstanceOf("Drupal\cache_register\Object\RegisterInterface", $drawer->getRegister());
    }
    else {
      $this->assertEquals(NULL, $drawer->getRegister());
    }
  }

  /**
   * @covers ::openRegister
   */
  public function testOpenRegister() {
    $manager = $this->getManager();
    $register = $manager->openRegister('cache_register_test', 'drawer_id');
    $this->assertInstanceOf("Drupal\cache_register\Object\RegisterInterface", $register);
  }

}
