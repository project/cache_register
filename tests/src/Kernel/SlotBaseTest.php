<?php

namespace Drupal\Tests\cache_register\Kernel;

use Drupal\cache_register\Object\SlotBaseInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Tests the SlotBase class.
 *
 * @coversDefaultClass \Drupal\cache_register\Object\SlotBase
 * @group cache_register
 */
class SlotBaseTest extends CacheRegisterKernelTestBase {

  /**
   * Ensure slot cache persists reopening.
   */
  public function testReopenSlot() {
    /** @var \Drupal\cache_register\Object\SlotBase $slot */
    $slot_1 = $this->createSlotBase('testReopenSlot');
    $cache_data = 'cached_data';
    $this->callMethod(
      $slot_1,
      'doSetCache',
      [$cache_data, -1, []]
    );
    $slot_cache_1 = $slot_1->getCache();

    // Reopen the slot.
    $slot_2 = $this->createSlotBase('testReopenSlot');
    $slot_cache_2 = $slot_2->getCache();
    $this->assertEquals($slot_cache_1, $slot_cache_2);
  }

  /**
   * @covers ::getDrawer
   */
  public function testGetDrawer() {
    $drawer = $this->createDrawer();
    $slot = $this->createSlotBase('slot', $drawer);
    $this->assertEquals($drawer, $slot->getDrawer());
  }

  /**
   * @covers ::getRegister
   * @dataProvider boolProvider
   */
  public function testGetRegister($open_register) {
    $drawer = $this->createDrawer('d', $open_register);
    $slot = $this->createSlotBase('slot', $drawer);

    if (!$open_register) {
      $this->assertEquals(NULL, $slot->getRegister());
    }
    else {
      $this->assertInstanceOf('Drupal\cache_register\Object\Register', $slot->getRegister());
    }
  }

  /**
   * @covers ::getCacheBackend
   */
  public function testGetCacheBackend() {
    $slot = $this->createSlotBase('testGetCacheBackend');
    $this->assertEquals($slot->getDrawer()->getCacheBackend(), $slot->getCacheBackend());
    $this->assertInstanceOf('Drupal\Core\Cache\CacheBackendInterface', $slot->getCacheBackend());
  }

  /**
   * @covers ::doSetCache
   * @dataProvider cacheDataProvider
   *
   * @param int $data
   *   Data value.
   * @param int $expire
   *   Make sure this is a valid timestamp or errors will throw.
   *   Cannot rely on strtotime or associated approaches.
   * @param array $tags
   *   Array value.
   */
  public function testDoSetCache($data, int $expire = CacheBackendInterface::CACHE_PERMANENT, array $tags = []) {
    $slot = $this->createSlotBase('testIsCached');
    $this->commonTestSetCache($slot, $data, $expire, $tags, 'doSetCache');
  }

  /**
   * @covers ::isCached
   */
  public function testIsCached() {
    // Before caching.
    $slot = $this->createSlotBase('testIsCached');
    $this->assertEquals(FALSE, $slot->isCached());

    // After caching.
    $slot->getCacheBackend()->set($slot->id(), 'cache_data');
    $this->assertEquals(TRUE, $slot->isCached());

    // After invalidating the cache.
    $slot->getCacheBackend()->invalidate($slot->id());
    $this->assertEquals(FALSE, $slot->isCached());

    // Repopulate and then delete the cache.
    $slot->getCacheBackend()->set($slot->id(), 'cache_data');
    $slot->getCacheBackend()->delete($slot->id());
    $this->assertEquals(FALSE, $slot->isCached());
  }

  /**
   * @covers ::getCache
   */
  public function testGetCache() {
    // Before caching.
    $slot_id = 'testGetCache';
    $slot = $this->createSlotBase($slot_id);
    $slot_twin = $this->createSlotBase($slot_id);
    $this->assertEquals(NULL, $slot->getCache());
    $this->assertEquals($slot->getCacheBackend()->get($slot->id()), $slot->getCache());
    $this->assertEquals($slot->getCache(), $slot_twin->getCache());

    // After caching.
    $slot->getCacheBackend()->set($slot->id(), 'cache_data');
    $this->assertNotEquals($slot->getCache(), NULL);
    $this->assertEquals($slot->getCacheBackend()->get($slot->id()), $slot->getCache());
    $this->assertEquals($slot->getCache(), $slot_twin->getCache());

    // After invalidating the cache.
    $slot->getCacheBackend()->invalidate($slot->id());
    $this->assertEquals(NULL, $slot->getCache());
    $this->assertNotEquals($slot->getCache(TRUE), NULL);
    $this->assertEquals($slot->getCacheBackend()->get($slot->id()), $slot->getCache());
    $this->assertEquals($slot->getCache(), $slot_twin->getCache());
    $this->assertEquals($slot->getCache(TRUE), $slot_twin->getCache(TRUE));

    // Repopulate and then delete the cache.
    $slot->getCacheBackend()->set($slot->id(), 'cache_data');
    $slot->getCacheBackend()->delete($slot->id());
    $this->assertEquals(NULL, $slot->getCache());
    $this->assertEquals($slot->getCacheBackend()->get($slot->id()), $slot->getCache());
    $this->assertEquals($slot->getCache(), $slot_twin->getCache());

  }

  /**
   * @covers ::doGetCacheData
   */
  public function testDoGetCacheData() {
    $slot_id = 'testIsCached';
    $slot = $this->createSlotBase($slot_id);
    $slot_twin = $this->createSlotBase($slot_id);
    $cid = $slot->id();
    $this->assertEquals(NULL, $slot->getCache());

    // After caching.
    $cache_data = 'cache_data';
    $slot->getCacheBackend()->set($cid, $cache_data);
    $this->assertNotEquals($this->doGetCacheData($slot), NULL);
    $this->assertDoGetCacheData($slot, $slot_twin, $cache_data);

    // After invalidating the cache.
    $slot->getCacheBackend()->invalidate($slot->id());
    $this->assertDoGetCacheData($slot, $slot_twin, NULL);

    // Not sure why, but allowing invalid with this helper method throws:
    //  Fatal error: Uncaught LogicException: The database connection is not serializable.
    //   This probably means you are serializing an object that has an indirect reference
    //   to the database connection. Adjust your code so that is not necessary. Alternatively,
    //   look at DependencySerializationTrait as a temporary solution. in
    //   /var/www/html/web/core/lib/Drupal/Core/Database/Connection.php:2035
    //
    // Also tried using DependencySerializationTrait, but it didn't help.
    // As such, just need to duplicate that method here manually to test
    // allowing invalidated entries.
    // $this->assertDoGetCacheData($slot, $slot_twin, $cache_data, TRUE);

    $this->assertEquals(
      $cache_data,
      $slot->getCache(TRUE)->data
    );
    $this->assertEquals(
      $slot->getCache(TRUE)->data,
      $this->doGetCacheData($slot, TRUE)
    );
    $this->assertEquals(
      $this->doGetCacheData($slot, TRUE),
      $this->doGetCacheData($slot_twin, TRUE),
    );

    // Repopulate the cache with a different value.
    $cache_data_2 = 'cache_data_2';
    $slot->getCacheBackend()->set($cid, $cache_data_2);
    $this->assertDoGetCacheData($slot, $slot_twin, $cache_data_2);


    // Delete the cache entry.
    $slot->getCacheBackend()->delete($cid);
    $this->assertDoGetCacheData($slot, $slot_twin, NULL);
  }

  /**
   * Runs assertions for testDoGetCacheData().
   *
   * @param \Drupal\cache_register\Object\SlotBaseInterface $slot
   *   The initial slot.
   * @param \Drupal\cache_register\Object\SlotBaseInterface $slot_twin
   *   This initial slot's twin.
   * @param string|null $expected_value
   *   The value expected to be present in the cache.
   * @param bool $allow_invalid
   *   Whether to allow invalidated cache entries.
   *
   * @return void
   * @throws \ReflectionException
   */
  private function assertDoGetCacheData(SlotBaseInterface $slot, SlotBaseInterface $slot_twin, ?string $expected_value, bool $allow_invalid = FALSE) {
    $real_cached_data = is_null($slot->getCache($allow_invalid)) ? NULL : $slot->getCache($allow_invalid)->data;

    $this->assertEquals($expected_value, $real_cached_data);
    $this->assertEquals($real_cached_data, $this->doGetCacheData($slot));
    $this->assertEquals($this->doGetCacheData($slot), $this->doGetCacheData($slot_twin));
  }


  /**
   * @covers ::addCacheTags
   * @dataProvider cacheTagsProvider
   */
  public function testAddCacheTags($tags_to_add, $is_valid) {
    $slot = $this->createSlotBase('testAddCacheTags');
    $this->doTestAddCacheTags($slot, $tags_to_add, $is_valid);
  }

  /**
   * Gets the tags that are set on a slot's cache.
   *
   * @param \Drupal\cache_register\Object\SlotBaseInterface $slot
   *   The slot.
   *
   * @return array
   *   The tags.
   */
  private function getTagsFromSlotCacheEntry(SlotBaseInterface $slot): array {
    $this->assertNotEquals(NULL, $slot->getCache());
    return $slot->getCache()->tags;
  }

  /**
   * Provides data to test id().
   *
   * Leave this and testId() down here at the bottom
   * so we don't need to scroll past all this junk
   * whenever we need to go through this class.
   *
   * @return array[]
   *   Returns the array values.
   */
  public function idProvider(): array {
    return [
      // No register.
      '1 id string, no register' => [
        self::MOCK_DRAWER_ID,
        'id_1',
        self::MOCK_DRAWER_ID . ':id_1',
      ],
      '1 id int, no register' => [
        self::MOCK_DRAWER_ID,
        1,
        self::MOCK_DRAWER_ID . ':1',
      ],
      '1 id array, no register' => [
        self::MOCK_DRAWER_ID,
        ['id_1'],
        self::MOCK_DRAWER_ID . ':id_1',
      ],
      '2 id array, no register' => [
        self::MOCK_DRAWER_ID,
        ['id_1', 'id_2'],
        self::MOCK_DRAWER_ID . ':id_1.id_2',
      ],
      '2 int id array, no register' => [
        self::MOCK_DRAWER_ID,
        [1, 2],
        self::MOCK_DRAWER_ID . ':1.2',
      ],
      '2 mixed id array, no register' => [
        self::MOCK_DRAWER_ID,
        [1, '2'],
        self::MOCK_DRAWER_ID . ':1.2',
      ],
      '3 id array, no register' => [
        self::MOCK_DRAWER_ID,
        ['id_1', 'id_2', 'id_3'],
        self::MOCK_DRAWER_ID . ':id_1.id_2.id_3',
      ],
      // Invalid.
      'object ID, no register' => [
        self::MOCK_DRAWER_ID,
        new \stdClass(),
        NULL,
      ],
      'bool ID, no register' => [
        self::MOCK_DRAWER_ID,
        FALSE,
        NULL,
      ],
      'object array ID, no register' => [
        self::MOCK_DRAWER_ID,
        [new \stdClass()],
        NULL,
      ],
      'mixed invalid array ID, no register' => [
        self::MOCK_DRAWER_ID,
        ['string', new \stdClass()],
        NULL,
      ],
    ];
  }

  /**
   * @covers ::id
   * @dataProvider idProvider
   */
  public function testId($drawer_id, $slot_ids, $expected_id) {
    $drawer = $this->createDrawer($drawer_id);

    if (is_null($expected_id)) {
      $this->expectException('TypeError');
    }

    $slot = $this->createSlotBase($slot_ids, $drawer);
    $this->assertEquals($expected_id, $slot->id());
  }

}
